#include <stdio.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <Hash.h>
#include <Servo.h>
#define HTTP_REST_PORT 8000
#define WIFI_RETRY_DELAY 500
#define MAX_WIFI_INIT_RETRY 50
#define SECRET "ASDZXCQWE"
Servo myservo;


const char* wifi_ssid = "Thunder2";
const char* wifi_passwd = "KrIsS1/2/3";
String node_key;
byte lock_action;

ESP8266WebServer http_rest_server(HTTP_REST_PORT);

//******** server & json  ******************//

void get_lock_status() {
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& jsonObj = jsonBuffer.createObject();
    char JSONmessageBuffer[200];

    jsonObj["lock_action"] = lock_action;
    jsonObj.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    http_rest_server.send(200, "application/json", JSONmessageBuffer);
}

bool json_to_resource(JsonObject& jsonBody) {
  lock_action = jsonBody["lock_action"];
  String key =jsonBody["key"].as<String>();
  Serial.println(key);
  Serial.println(node_key);
  if (key!=node_key){
    return false;
  }
  if(lock_action==1){
    digitalWrite(LED_BUILTIN, LOW);
    myservo.write(150);
  }
  else{
    digitalWrite(LED_BUILTIN, HIGH);
    myservo.write(10);
  }
  
  Serial.println(lock_action);
  return true;
  
}

void post_lock_action() {
  StaticJsonBuffer<500> jsonBuffer;
  
  JsonObject& jsonBody = jsonBuffer.parseObject(http_rest_server.arg("plain"));

  if (!jsonBody.success()) {
    Serial.println("error in parsin json body");
    http_rest_server.send(400);
  }
  else {
    if (http_rest_server.method() == HTTP_POST) {
      bool status = json_to_resource(jsonBody);
      
      if (status){
      //http_rest_server.sendHeader("Action", "/lock_action/" + lock_action);
      http_rest_server.send(201);
      }
      else{
        http_rest_server.send(401);
      }
    }
  }
}

void config_rest_server_routing() {
  http_rest_server.on("/lock_action", HTTP_GET, get_lock_status);
  http_rest_server.on("/lock_action", HTTP_POST, post_lock_action);
}

//******** setup & wifi  ******************//

int init_wifi() {
  int retries = 0;

  Serial.println("Connecting to WiFi AP..........");

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_passwd);
  // check the status of WiFi connection to be WL_CONNECTED
  while ((WiFi.status() != WL_CONNECTED) && (retries < MAX_WIFI_INIT_RETRY)) {
    retries++;
    delay(WIFI_RETRY_DELAY);
    Serial.print("#");
  }
  return WiFi.status(); // return the WiFi connection status
}

void setup(void) {
  node_key=sha1(SECRET+WiFi.macAddress());
  myservo.attach(0);
  pinMode(LED_BUILTIN, OUTPUT); 
  Serial.begin(9600);

  if (init_wifi() == WL_CONNECTED) {
    Serial.print("Connetted to ");
    Serial.print(wifi_ssid);
    Serial.print("--- IP: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.print("Error connecting to: ");
    Serial.println(wifi_ssid);
  }

  config_rest_server_routing();

  http_rest_server.begin();
  Serial.println("HTTP REST Server Started");

 
  Serial.println();
  Serial.print("Mac address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("SHA of mac address: ");
  Serial.print(sha1(WiFi.macAddress()));
}

void loop(void) {
  http_rest_server.handleClient();
}
